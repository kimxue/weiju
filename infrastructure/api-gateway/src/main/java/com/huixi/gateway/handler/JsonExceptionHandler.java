package com.huixi.gateway.handler;

import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.ResourceProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yeqiu
 */
public class JsonExceptionHandler extends DefaultErrorWebExceptionHandler {

    public JsonExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties, ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
    }

    /**
     * 1、指定使用json格式进行响应
     *
     * @param errorAttributes
     * @return
     */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        //参数1：匹配所有的请求(所有出现异常的请求)  参数2：指定自己设置响应(默认转为json)
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }


    @Override
    protected Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        // 响应报文的参数：code  message  data     如果需要设置R对象：我们需要封装R对象的属性设置到data属性中
        Map<String, Object> map = new HashMap<>(1);
        //服务器内部错误
        map.put("code", 500);
        Throwable error = getError(request);
        if (error instanceof NotFoundException) {
            map.put("code", 404);
        }
        map.put("message", error.getMessage());

        return map;
    }


    /**
     * 3、提供响应报文的状态码
     *
     * @param errorAttributes
     * @return
     */
    @Override
    protected int getHttpStatus(Map<String, Object> errorAttributes) {
        // errorAttributes代表上面方法中封装的响应数据
        int code = (int) errorAttributes.get("code");
        return HttpStatus.valueOf(code).value();
    }





}

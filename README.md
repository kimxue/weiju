## 汇溪和他们的小伙伴们

> **汇溪**： 汇溪成海之意，象征汇聚多方力量。



时间 **2021年3月29日15:39:21**   小程序端方面准备用**uni-app** 重构，短时间内后台变动会比较大，不建议拉下来学习。



# 项目框架&工具

考虑到这是一个 社交性质 的软件，信息交流比较大，采用微服务，也满足我的一个心愿，工作以来一直没用到过。



- 推荐工具
  - IntelliJ IDEA Ultimate
  - DataGrip —— 数据库管理工具
  - FinalShell —— ssh连接工具
- 后台
  - Java 1.8
  - SpringBoot 2.3.0.RELEASE
  - SpringCloud Hoxton.SR10
  - SpringSecurity 1.0.9.RELEASE
  - spring-cloud-starter-alibaba-nacos-config 2.2.2.RELEASE
  - wxJava 4.0.0
  - hutool  5.1.1
  - Mybatis-Plus 3.3.0
- 第三方服务
  - 阿里云 OSS



# 项目结构

```
microspur
├─ pom.xml	maven配置文件
├─ README.md	说明文件
├─ infrastructurn 基础项目
│  ├─ api-gateway 项目网关
│  └─ mybatis-generator 代码生成
├─ microspur-commons 项目公告模块
│  ├─ commons-utils 通用帮助类
│  ├─ service-base 项目服务基础类
│  │  ├─ src ...
│  │  │  ├─ base
│  │  │  │  ├─ BaseEntity.java 实体类继承类
│  │  │  │  └─ BaseController.java controller继承类  
│  │  │  └─ config
│  │  │  │  ├─ FieldMetaObjectHandler.java 公共字段，自动填充值
│  │  │  │  └─ MybatisPlusConfig.java 分页配置 ...
├─ microspur-service 服务模块
│  ├─ service-** 多个微服务模块
```





# 开发日志

- 2021年5月13日09:35:14

  做出一个预览静态界面的版本，界面完成度50%，可以下载发行版进行预览。

- 2021年5月12日09:33:15

  静态界面完成大半了，后台那边没动🤣


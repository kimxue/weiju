package com.huixi.oss.controller;

import com.huixi.commonutils.errorcode.ErrorCodeEnum;
import com.huixi.commonutils.exception.BusinessException;
import com.huixi.commonutils.util.wrapper.ResultData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *  上传阿里oss 存储库 相关接口
 * @Author 叶秋 
 * @Date 2021/4/10 14:45
 **/
@RestController
@Slf4j
public class OssController {



    /**
     *  测试
     * @Author 叶秋
     * @Date 2021/4/10 14:49
     * @return void
     **/
    @GetMapping(value = "helloOss")
    public ResultData helloOss(){
        log.info("hello oss");
        return ResultData.ok("hello oss");
    }

}

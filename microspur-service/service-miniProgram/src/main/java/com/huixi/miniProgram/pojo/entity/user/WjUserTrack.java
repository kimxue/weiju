package com.huixi.miniProgram.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.servicebase.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户足迹表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_track")
public class WjUserTrack extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId(value = "user_track_id", type = IdType.AUTO)
    private Integer userTrackId;

    @TableField("user_id")
    private Integer userId;

    @TableField("user_name")
    private String userName;

    @TableField("ip")
    private String ip;

    @TableField("type")
    private String type;

    @TableField("text")
    private String text;

    @TableField("param")
    private String param;

    @TableField("create_by")
    private String createBy;

    @TableField("update_by")
    private String updateBy;

    @TableField("t_flag")
    private Integer tFlag;

    @TableField("target_id")
    private String targetId;

    @TableField("brand")
    private String brand;

    @TableField("model")
    private String model;

    @TableField("version")
    private String version;

    @TableField("system")
    private String system;

    @TableField("platform")
    private String platform;


}

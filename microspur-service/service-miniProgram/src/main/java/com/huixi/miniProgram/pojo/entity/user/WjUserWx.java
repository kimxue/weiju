package com.huixi.miniProgram.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_wx")
public class WjUserWx implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId( value = "user_id", type = IdType.INPUT)
    private Integer userId;

    @TableField("wx_open_id")
    private String wxOpenId;

    @TableField("wx_union_id")
    private String wxUnionId;

    @TableField("wx_app_id")
    private String wxAppId;


}

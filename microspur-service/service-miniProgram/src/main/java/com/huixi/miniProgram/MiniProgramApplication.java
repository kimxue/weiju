package com.huixi.miniProgram;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author yeqiu
 */
@EnableFeignClients
@EnableCaching
@EnableAsync
@EnableTransactionManagement
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(value = "com.huixi.miniProgram.mapper")
@ComponentScan(basePackages = {"com.huixi"})
public class MiniProgramApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiniProgramApplication.class, args);
    }

}

package com.huixi.miniProgram.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.miniProgram.mapper.WjUserWxMapper;
import com.huixi.miniProgram.pojo.entity.user.WjUserWx;
import com.huixi.miniProgram.service.WjUserWxService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 专门用来存储微信后台发送给我们的数据 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjUserWxServiceImpl extends ServiceImpl<WjUserWxMapper, WjUserWx> implements WjUserWxService {



}

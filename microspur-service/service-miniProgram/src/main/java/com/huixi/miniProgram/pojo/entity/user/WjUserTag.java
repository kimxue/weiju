package com.huixi.miniProgram.pojo.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.servicebase.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *  WjUserTag对象
 *
 * @author 叶秋
 * @since 2020-07-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_user_tag")
public class WjUserTag extends BaseEntity implements Serializable  {

    private static final long serialVersionUID=1L;

    @TableId( value = "user_id", type = IdType.AUTO)
    private Integer userTagId;

    @TableId("user_id")
    private String userId;

    @TableField("tag_id")
    private Integer tagId;

    @TableField("tag_value")
    private String tagValue;

    @TableField("is_check")
    private Boolean isCheck;

    @TableField("create_by")
    private String createBy;

    @TableField("update_by")
    private String updateBy;



}

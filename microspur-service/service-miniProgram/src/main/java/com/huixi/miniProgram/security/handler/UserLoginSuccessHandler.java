package com.huixi.miniProgram.security.handler;

import com.huixi.commonutils.util.ResultUtil;
import com.huixi.commonutils.util.wrapper.ResultData;
import com.huixi.miniProgram.config.JWTConfig;
import com.huixi.miniProgram.pojo.entity.user.WjUser;
import com.huixi.miniProgram.util.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description 登录成功处理类
 * @Author Sans
 * @CreateTime 2019/10/3 9:13
 */
@Slf4j
@Component
public class UserLoginSuccessHandler implements AuthenticationSuccessHandler {

    /**
     * 登录成功返回结果
     * @Author Sans
     * @CreateTime 2019/10/3 9:27
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication){
        // 组装JWT
        WjUser wjUser =  (WjUser) authentication.getPrincipal();
        String token = JwtTokenUtil.createAccessToken(wjUser);
        token = JWTConfig.tokenPrefix + token;

        Map<String, Object> map = new HashMap<>();
        map.put("token", token);
        map.put("wjUser", wjUser);

        ResultUtil.responseJson(response, ResultData.ok(map));


    }


}

package com.huixi.miniProgram.service;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.miniProgram.pojo.entity.user.WjUser;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserService extends IService<WjUser> {

    /**
     * 根据code 获取session_key、open_id
     *
     * @param code 用户code
     * @return java.lang.String
     * @Author 李辉
     * @Date 2019/11/23 4:31
     **/
    WxMaJscode2SessionResult getWxSession(String code);


    /**
     * 根据用户code 获取用户的 openId 和 sessionKey , 来确定有没有授权过。 如果授权发放用户信息 ，没有就创建一个用户
     *
     * @param code 用户小程序的code
     * @return WjUser 用户信息类
     * @Author 叶秋
     * @Date 2020/2/1 20:17
     **/
    WjUser detectionUserAuthorization(String code);



}

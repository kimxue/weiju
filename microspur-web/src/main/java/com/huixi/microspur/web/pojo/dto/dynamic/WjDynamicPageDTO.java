package com.huixi.microspur.web.pojo.dto.dynamic;

import com.huixi.commonutils.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 *  动态 接收前端的值
 * @Author 叶秋 
 * @Date 2020/4/19 21:14
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="分页查询动态 传给前端的值")
public class WjDynamicPageDTO implements Serializable {

    @ApiModelProperty(value = "模糊搜索内容")
    private String content;

    @ApiModelProperty(value = "创建时间")
    public Boolean createTime;

    @ApiModelProperty(value = "动态 点赞数||赞同数")
    private Boolean endorseCount;

    @ApiModelProperty(value = "用户id", hidden = true)
    private String userId;

    @ApiModelProperty("分页参数")
    private PageQuery pageQuery;



}

package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicEndorse;

/**
 *  动态点赞表 服务类
 * @Author 叶秋 
 * @Date 2020/4/20 22:06
 * @param 
 * @return 
 **/
public interface WjDynamicEndorseService extends IService<WjDynamicEndorse> {

    /**
     *  判断用户是否点赞
     * @Author 叶秋
     * @Date 2020/3/19 0:15
     * @param dynamicId 动态id
     * @param userId 用户id
     * @return java.lang.Boolean
     **/
    Boolean isEndorse(String dynamicId, String userId);

    /**
     *  获取改诉求的总点赞数
     * @Author 叶秋
     * @Date 2020/3/19 0:25
     * @param dynamicId 动态id
     * @return int
     **/
    int getTotleCount(String dynamicId);

    /**
     *  取消诉求点赞
     * @Author 叶秋
     * @Date 2020/3/20 0:37
     * @param wjAppealEndorse
     * @return java.lang.Boolean
     **/
    Boolean cancelEndorse(WjDynamicEndorse wjAppealEndorse);

}

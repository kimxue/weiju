package com.huixi.microspur.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealAddress;

/**
 *  诉求地址 mapper 接口
 * @Author 叶秋
 * @Date 2020/3/25 2:21
 * @param
 * @return
 **/
public interface WjAppealAddressMapper extends BaseMapper<WjAppealAddress> {
}

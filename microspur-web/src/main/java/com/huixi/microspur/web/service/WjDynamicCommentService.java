package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamicComment;

/**
 * <p>
 * 动态评论表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjDynamicCommentService extends IService<WjDynamicComment> {

}

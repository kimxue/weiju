package com.huixi.microspur.web.controller.appeal;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huixi.servicebase.base.BaseController;
import com.huixi.commonutils.page.PageData;
import com.huixi.commonutils.page.PageFactory;
import com.huixi.commonutils.page.PageQuery;
import com.huixi.commonutils.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.dto.appeal.MyAppealCommentPageDTO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealCommentDTO;
import com.huixi.microspur.web.pojo.dto.appeal.WjAppealCommentPageDTO;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealComment;
import com.huixi.microspur.web.pojo.entity.user.WjUser;
import com.huixi.microspur.web.pojo.vo.appeal.QueryAppealCommentVO;
import com.huixi.microspur.web.service.WjAppealCommentService;
import com.huixi.microspur.web.service.WjUserService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 诉求-评论 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjAppealComment")
@Api(tags = "诉求-评论模块")
public class WjAppealCommentController extends BaseController {


    @Resource
    private WjAppealCommentService wjAppealCommentService;

    @Resource
    private WjUserService wjUserService;


    /**
     *  添加诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param appealCommentDTO
     * @return com.huixi.commonutils.util.wrapper.Wrapper
     **/
    @PostMapping("/appealComment")
    @ApiOperation(value = "添加诉求评论")
    public Wrapper appealComment(@Valid @RequestBody WjAppealCommentDTO appealCommentDTO){

        String nowUserId = CommonUtil.getNowUserId();
        appealCommentDTO.setUserId(nowUserId);

        WjAppealComment wjAppealComment = new WjAppealComment();

        BeanUtil.copyProperties(appealCommentDTO, wjAppealComment);

        boolean save = wjAppealCommentService.save(wjAppealComment);

        return Wrapper.ok(save);

    }



    /**
     *  删除 诉求评论
     * @Author 叶秋
     * @Date 2020/4/13 22:23
     * @param appealCommentId 诉求id
     * @return com.huixi.commonutils.util.wrapper.Wrapper
     **/
    @DeleteMapping("/appealComment/{appealCommentId}")
    @ApiOperation(value = "删除 诉求评论")
    @ApiImplicitParam(name = "appealCommentId", value = "诉求评论的id", dataType = "String"
    ,required = true)
    public Wrapper appealComment(@PathVariable String appealCommentId){

        boolean b = wjAppealCommentService.removeById(appealCommentId);

        return Wrapper.ok(b);

    }


    /**
     *  分页查询我的诉求评论
     * @Author 叶秋
     * @Date 2020/6/18 22:38
     * @param myAppealCommentPageDTO
     * @return com.huixi.commonutils.util.wrapper.Wrapper
     **/
    @PostMapping("/pageMyAppealComment")
    @ApiOperation(value = "分页查询我的诉求评论")
    public Wrapper pageMyAppealComment(@Valid @RequestBody MyAppealCommentPageDTO myAppealCommentPageDTO){

        PageData<Object> objectPageData = new PageData<>();

        Page<WjAppealComment> page = PageFactory.createPage(myAppealCommentPageDTO.getPageQuery());

        LambdaQueryWrapper<WjAppealComment> eq = Wrappers.<WjAppealComment>lambdaQuery()
                .eq(WjAppealComment::getUserId, myAppealCommentPageDTO.getUserId());

        Page<WjAppealComment> wjAppealCommentPage = wjAppealCommentService.page(page, eq);


        BeanUtil.copyProperties(wjAppealCommentPage, objectPageData);


        return Wrapper.ok(objectPageData);

    }


    /**
     *  分页查询诉求的 评论
     * @Author 叶秋
     * @Date 2020/4/13 22:48
     * @param wjAppealCommentPageDTO
     * @return com.huixi.commonutils.util.wrapper.Wrapper
     **/
    @PostMapping("/pageAppealComment")
    @ApiOperation(value = "分页查询诉求的 评论")
    public Wrapper queryPageAppealComment(@Valid @RequestBody WjAppealCommentPageDTO wjAppealCommentPageDTO) {

        PageData<QueryAppealCommentVO> objectPageData = new PageData<>();
        List<QueryAppealCommentVO> queryAppealCommentVOS = new ArrayList<>();

        // 查询出来的评论
        Page<WjAppealComment> page = PageFactory.createPage(wjAppealCommentPageDTO.getPageQuery());
        QueryWrapper<WjAppealComment> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.orderByDesc("create_time").eq("appeal_id", wjAppealCommentPageDTO.getAppealId());
        Page<WjAppealComment> wjAppealCommentPage = wjAppealCommentService.page(page, objectQueryWrapper);

        // 查询出用户
        List<WjAppealComment> records = wjAppealCommentPage.getRecords();
        Set<String> userIds = records.stream().map(WjAppealComment::getUserId).collect(Collectors.toSet());
        List<WjUser> userList =
                wjUserService.lambdaQuery().in(CollUtil.isNotEmpty(userIds), WjUser::getUserId, userIds).list();


        QueryAppealCommentVO queryAppealCommentVO;
        for (WjAppealComment record : records) {
            queryAppealCommentVO = new QueryAppealCommentVO();
            BeanUtil.copyProperties(record, queryAppealCommentVO);

            WjUser wjUser = userList.stream().filter(e -> e.getUserId().equals(record.getUserId())).findAny().orElseGet(WjUser::new);
            queryAppealCommentVO.setNickName(wjUser.getNickName()).setHeadPortrait(wjUser.getHeadPortrait());

            queryAppealCommentVOS.add(queryAppealCommentVO);
        }

        BeanUtil.copyProperties(wjAppealCommentPage, objectPageData);
        objectPageData.setRecords(queryAppealCommentVOS);


        return Wrapper.ok(objectPageData);

    }






}


package com.huixi.microspur.web.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.web.pojo.entity.chat.WjChatUser;

/**
 * <p>
 * 聊天室对应的用户 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatUserService extends IService<WjChatUser> {

}

package com.huixi.microspur.web.pojo.dto.appeal;

import com.huixi.commonutils.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  用来 查询其他人 诉求的实体类
 * @Author 叶秋 
 * @Date 2020/6/30 21:34
 * @param 
 * @return 
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用来 查询其他人 诉求的实体类", description = "用来 查询其他人 诉求的实体类")
public class WjAppealByUserDTO {

    @ApiModelProperty(value = "要被查询诉求的用户id")
    private String userId;

    private PageQuery pageQuery;

}

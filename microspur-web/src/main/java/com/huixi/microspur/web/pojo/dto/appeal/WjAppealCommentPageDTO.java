package com.huixi.microspur.web.pojo.dto.appeal;

import com.huixi.commonutils.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *  诉求评论 分页VO类
 * @Author 叶秋
 * @Date 2020/4/13 22:45
 * @param
 * @return
 **/
@Data
@ApiModel(value = "诉求分页查询所需的", description = "诉求分页查询所需的, 分页不传有默认值")
public class WjAppealCommentPageDTO {

    @NotNull(message = "诉求id为空")
    @ApiModelProperty(value = "诉求id", required = true)
    private String appealId;


    private PageQuery pageQuery;


}

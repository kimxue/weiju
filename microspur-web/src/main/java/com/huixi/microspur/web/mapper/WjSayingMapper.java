package com.huixi.microspur.web.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.web.pojo.entity.saying.WjSaying;

/**
 * <p>
 * 语录表 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjSayingMapper extends BaseMapper<WjSaying> {

}

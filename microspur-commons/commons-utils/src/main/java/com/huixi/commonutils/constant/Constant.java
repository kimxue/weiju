package com.huixi.commonutils.constant;

/**
 * 常量
 * @author 叶秋
 * @Date 2021年4月10日14:44:57
 */
public interface Constant {

    /**
     *  删除标识 N 没有删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    String FALSE = "0";

    /**
     *  删除标识 Y 标识已经删除
     * @Author 叶秋
     * @Date 2020/5/5 11:04
     **/
    String TRUE = "1";



    /**
     * 诉求模块
     **/
    String APPEAL_MATERIAL = "appeal";

    /**
     * 动态模块
     **/
    String DYNAMIC_MATERIAL = "dynamic";




}
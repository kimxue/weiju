package com.huixi.commonutils.page;

import lombok.Data;

/**
 * 分页查询的请求参数封装
 *
 * @author 碧海青天夜夜心
 * @sice 2019年10月15日 16:05:17
 */
@Data
public class PageQuery {

    /**
     * 每页的条数
     */
    private Integer pageSize;

    /**
     * 页编码(第几页)
     */
    private Integer pageNo;


    public PageQuery() {
    }

    public PageQuery(Integer pageSize, Integer pageNo, String sort, String orderByField) {
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }
}
